﻿using System.Xml;
using System.IO;
using System;
using System.Diagnostics;
using System.Data;
using System.Text;
using System.Configuration;

namespace Xsoft
{
    public class SimpleLog
    {                                
        /// <summary>
        /// Obtiene ruta, esta función hace que funcione para web y win forms 
        /// </summary>
        /// <returns>ruta</returns>
        private static string getRuta()
        {
            string r = null;                      
            try
            {
                // Directorio del archivo: Una key en el web.config "DirLog"
                if (System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["DirLog"]) != null)
                {
                    r = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["DirLog"]);
                }
                if (r == null)
                {
                    r = System.Web.HttpContext.Current.Server.MapPath(null);
                }
                if (r == null)
                {
                    r = System.IO.Directory.GetCurrentDirectory();
                }
            }
            catch
            {
                try
                {
                    r = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                }
                catch (Exception ex2)
                {
                    EventLog.WriteEntry("SimpleLog - getRuta", ex2.Message);

                }    
            }

            return r;
        }

        /// <summary>
        /// Tipos de entrada en el log
        /// </summary>
        public enum TipoDeEntrada { Evento, Excepción };

        /// <summary>
        /// Crea el archivo de log
        /// </summary>
        /// <param name="ComandoVar">Comando</param>
        /// <param name="DescriVar"></param>
        /// <param name="Elemento"></param>
        private static void LogCreate(string ComandoVar, string DescriVar, TipoDeEntrada Tipo)
        {

            string comando = ComandoVar;
            string Descri = DescriVar;
            string Fecha = System.DateTime.Now.Date.ToShortDateString();
            string Hora = System.DateTime.Now.TimeOfDay.ToString();

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.NewLineOnAttributes = true;
            try
            {

                if (!Directory.Exists(getRuta() ))
                {
                    Directory.CreateDirectory(getRuta());
                }


                using (XmlWriter writer = XmlWriter.Create(getRuta() + "\\log.xml", settings))
                {
                    writer.WriteStartDocument();

                    writer.WriteStartElement("Log");
                    writer.WriteStartElement(Tipo.ToString());
                    writer.WriteStartAttribute("Comando");
                    writer.WriteValue(comando);
                    writer.WriteEndAttribute();

                    writer.WriteStartAttribute("Descripción");
                    writer.WriteValue(Descri);
                    writer.WriteEndAttribute();

                    writer.WriteStartAttribute("Fecha");
                    writer.WriteValue(Fecha);
                    writer.WriteEndAttribute();

                    writer.WriteStartAttribute("Hora");
                    writer.WriteValue(Hora);
                    writer.WriteEndAttribute();

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Flush();
                }
            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("SimpleLog - LogCreate", ex.Message);
            }
        }

        /// <summary>
        /// Si ya existe el archivo de log agrega la entrada
        /// </summary>
        /// <param name="ComandoVar">Comando</param>
        /// <param name="DescriVar">Descripción</param>
        /// <param name="TipoDeEntrada">Tipo de entrada</param>
        private static void LogAdd(string ComandoVar, string DescriVar, TipoDeEntrada Tipo)
        {
            try
            {
                XmlDocument xml_Document = new XmlDocument();
                xml_Document.Load(getRuta() + "\\log.xml");

                XmlElement Evento = xml_Document.CreateElement(Tipo.ToString());
                XmlAttribute Comando = Evento.SetAttributeNode("Comando", null);
                XmlAttribute Descri = Evento.SetAttributeNode("Descripción", null);
                XmlAttribute Fecha = Evento.SetAttributeNode("Fecha", null);
                XmlAttribute Hora = Evento.SetAttributeNode("Hora", null);

                Comando.Value = ComandoVar;
                Descri.Value = DescriVar;
                Fecha.Value = System.DateTime.Now.Date.ToShortDateString();
                Hora.Value = System.DateTime.Now.TimeOfDay.ToString();

                xml_Document.DocumentElement.AppendChild(Evento);
                xml_Document.Save(getRuta() + "\\log.xml");
            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("SimpleLog - LogAdd", ex.Message);
            }


        }

        /// <summary>
        /// Método Log
        /// </summary>
        /// <param name="Comando">Comando</param>
        /// <param name="Descri">Descripción</param>
        /// <param name="TipoDeEntrada">Tipo de Entrada</param>
        public static void Log(string Comando, string Descripcion, TipoDeEntrada Tipo)
        {
            try
            {
                if (System.IO.File.Exists(getRuta() + "\\log.xml"))
                {
                    LogAdd(Comando, Descripcion, Tipo);
                }
                else
                {
                    LogCreate(Comando, Descripcion, Tipo);
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("SimpleLog - Log", ex.Message);
            }
        }
              
       
        /// <summary>
        /// Función para leer el log y pasarlo a DataTable
        /// </summary>      
        private static DataTable Log2Table(TipoDeEntrada Tipo, bool Filtrar)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("TipoEntrada", typeof(string));
            dt.Columns.Add("Comando", typeof(string));
            dt.Columns.Add("Descripción", typeof(string));
            dt.Columns.Add("Fecha", typeof(DateTime));

            XmlReader xmlReader = XmlReader.Create(getRuta() + "\\log.xml");
            while (xmlReader.Read())
            {                               
                if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == (Filtrar ? Tipo.ToString() : xmlReader.Name)))
                {
                    if (xmlReader.HasAttributes)
                    {
                        DataRow row = dt.NewRow();
                        row["TipoEntrada"] = xmlReader.Name;
                        row["Comando"] = xmlReader.GetAttribute("Comando");
                        row["Descripción"] = xmlReader.GetAttribute("Descripción");
                        row["Fecha"] = xmlReader.GetAttribute("Fecha") + " " + xmlReader.GetAttribute("Hora");
                        dt.Rows.Add(row);

                    }
                }
            }
            xmlReader.Dispose();
            return dt;
        }

        /// <summary>
        /// Método público para leer el log y pasarlo a DataTable
        /// </summary>       
        public static DataTable Log2DataTable()
        {
            return Log2Table(TipoDeEntrada.Evento, false);
        }

        /// <summary>
        /// Método público para leer el log y pasarlo a DataTable (sobrecarga para filtrar)
        /// </summary>       
        public static DataTable Log2DataTable(TipoDeEntrada Tipo)
        {
            return Log2Table(Tipo, true);
        }


    }
}
