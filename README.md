# SimpleLog #


### Summary ###
Genera un archivo en formato xml, que sirve de log para cualquier aplicación .NET

### Uso ###
    Log(Comando,Descripción,TipoDeEntrada)
### Ejemplo ###
##### VB.NET #####

    Xsoft.SimpleLog.Log("Ingreso", "Se registró la entrada del usuario x al sistema", Xsoft.SimpleLog.TipoDeEntrada.Evento)

#### C# ###
	 Xsoft.SimpleLog.Log("Excepción - Guardar", "mensaje de la excepción controlada", Xsoft.SimpleLog.TipoDeEntrada.Excepción);

### Tipos de entrada del log ###
	Xsoft.SimpleLog.TipoDeEntrada.Excepción
	Xsoft.SimpleLog.TipoDeEntrada.Evento

### Ejemplo de salida [log.xml] ###
    <?xml version="1.0" encoding="utf-8"?>
    <Log>
      <Evento Comando="Ingreso" Descripción="Se registró la entrada del usuario x al sistema" Fecha="29/11/2016" Hora="22:34:52.1846752" />
      <Excepción Comando="Excepción - Guardar" Descripción="mensaje de la excepción controlada" Fecha="29/11/2016" Hora="22:36:15.5854455" />
    </Log>

### Notas ###
* Tiene un método para leer el log [log.xml] y entregarlo en un datatable, se puede filtrar por los tipos de entrada.
#
 	Xsoft.SimpleLog.Log2DataTable()
    
### Update v2.0.2 ###
    * Personalizar el directorio en dónde se generará el log, agregar las siguientes líneas en el web.config del proyecto
    <appSettings>
      <add key="DirLog" value="~/App_Data"/>
    </appSettings>
    
### NuGet ###
* Install-Package SimpleLog.dll
* https://www.nuget.org/packages/SimpleLog.dll


### Contact ###
lacho.gutierrez@gmail.com


P.D. Let's go play !!!